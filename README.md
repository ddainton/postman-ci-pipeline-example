# postman-ci-pipeline-example


[![Build Status](https://img.shields.io/bitbucket/pipelines/ddainton/postman-ci-pipeline-example.svg)](https://bitbucket.org/ddainton/postman-ci-pipeline-example)

--- 

A very simple project to demonstrate using [Newman](https://github.com/postmanlabs/newman) to run a Postman collections through some of the different CI systems.

The collection and environment files have been taken from the [All-Things-Postman Repo](https://github.com/DannyDainton/All-Things-Postman).

## Bitbucket Pipelines

### How does it work Bitbucket Pipelines

All the magic happens in the `bitbucket.pipelines.yml` file. This is using the `Newman` Docker image to run the collection file.

```yml
image: postman/newman

pipelines:
  default:
    - step:
        script:
        - newman --version
        - newman run ./tests/Restful_Booker_Collection.postman_collection.json -e ./tests/Restful_Booker_Environment.postman_environment.json
```


---

If you have any questions, you can drop me a message on Twitter `@dannydainton`.
